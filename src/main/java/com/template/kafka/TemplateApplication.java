package com.template.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Date;

@SpringBootApplication
public class TemplateApplication {
	private final KafkaSender kafkaSender;

	public TemplateApplication(KafkaSender kafkaSender) {
		this.kafkaSender = kafkaSender;
	}

	public static void main(String[] args) {
		SpringApplication.run(TemplateApplication.class, args);
	}

	@PostConstruct
	void testKafka() {
        for (int i = 0; i < 10; i++) {
            kafkaSender.send(new Date().getTime(), "message - " + i);
        }
	}

}
