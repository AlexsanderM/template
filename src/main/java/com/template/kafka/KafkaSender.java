package com.template.kafka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaSender {
    private KafkaTemplate<Long, String> kafkaTemplate;

    public KafkaSender(KafkaTemplate<Long, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Value("${spring.kafka.producer.topic}")
    private String topic;

    public void send(Long vId, String message) {
        kafkaTemplate.send(topic, vId, message);
    }
}
