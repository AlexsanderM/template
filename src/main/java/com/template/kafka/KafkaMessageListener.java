package com.template.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaMessageListener {
    @KafkaListener(
            topics = "${spring.kafka.consumer.topic.name}"
    )
    public void listenMessage(ConsumerRecord<Long, String> record, Acknowledgment ack) {
        log.debug("listenMessage()");
        log.debug("record = {}", record);

        if (record.value() == null) {
            log.error("Ошибка десериализации");
            return;
        }

        log.info("key: " + record.key());
        log.info("value: " + record.value());

        ack.acknowledge();
    }
}

