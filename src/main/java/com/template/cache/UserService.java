package com.template.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class UserService {
    Map<Long, User> userMap = new HashMap<>();

    public User create(User user) {
        userMap.put(user.getId(), user);
        return user;
    }

    @Cacheable(value = "user", key = "#id")
    public User get(Long id) {
        log.info("getting user by id: {}", id);
        return userMap.get(id);
    }
}
