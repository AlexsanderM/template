package com.template.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import javax.annotation.PostConstruct;
import java.io.IOException;

@SpringBootApplication
@EnableCaching
public class CacheApp {
    @Autowired
    UserService userService;
    private static int[] arrIndexBas = new int[]{-1, -1};

    public static void main(String[] args) throws IOException {
        SpringApplication.run(CacheApp.class, args);
    }


    @PostConstruct
    void testCache() throws IOException {
    }
}
