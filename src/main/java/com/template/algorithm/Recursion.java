package com.template.algorithm;

import java.util.HashMap;
import java.util.List;

public class Recursion {
    private final HashMap<Character, String> numberPhoneMap = new HashMap<>(){{
        put('2', "abc");
        put('3', "def");
        put('4', "ghi");
        put('5', "jkl");
        put('6', "mno");
        put('7', "pqrs");
        put('8', "tuv");
        put('9', "wxyz");
    }};

    // Рекурсивный перебор вариантов
    /**
     * Рита по поручению Тимофея наводит порядок в правильных скобочных последовательностях (ПСП), состоящих только из круглых скобок ().
     * Для этого ей надо сгенерировать все ПСП длины 2n в алфавитном порядке —– алфавит состоит из ( и ) и открывающая скобка идёт раньше закрывающей.
     * Помогите Рите —– напишите программу, которая по заданному n выведет все ПСП в нужном порядке.
     * Рассмотрим второй пример. Надо вывести ПСП из четырёх символов. Таких всего две:
     * (())
     * ()()
     * (()) идёт раньше ()(), так как первый символ у них одинаковый, а на второй позиции у первой ПСП стоит (, который идёт раньше ).
     * Формат ввода
     * На вход функция принимает n — целое число от 0 до 10.
     * Формат вывода
     * Функция должна напечатать все возможные скобочные последовательности заданной длины в алфавитном (лексикографическом) порядке.
     * Пример 1
     * Ввод	                 Вывод
     * 3                    ((()))
     *                      (()())
     *                      (())()
     *                      ()(())
     *                      ()()()
     *  BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
     *  int count = Integer.parseInt(buf.readLine());
     *  printBrac(count, 0, "");
     * */
    public void printBrac(int countOpen, int countClose, String pre) {
        if (countOpen == 0 && countClose == 0) {
            System.out.print(pre);
            System.out.println();
        }
        else {
            if (countOpen > 0) {
                printBrac(countOpen - 1, countClose + 1, pre + "(");
            }
            if (countClose > 0) {
                printBrac(countOpen, countClose - 1, pre + ")");
            }
        }
    }

    /**
     * На клавиатуре старых мобильных телефонов каждой цифре соответствовало несколько букв. Примерно так:
     * 2:'abc', 3:'def', 4:'ghi', 5:'jkl', 6:'mno', 7:'pqrs', 8:'tuv', 9:'wxyz'
     * Вам известно в каком порядке были нажаты кнопки телефона, без учета повторов. Напечатайте все комбинации букв,
     * которые можно набрать такой последовательностью нажатий.
     * Ввод	      Вывод
     * 23         ad ae af bd be bf cd ce cf
     *
     * List<String> list = new ArrayList<>();
     * printCharByPhone(str, "", list);
     * System.out.println(String.join(" ", list));
     */
    public void printCharByPhone(String number, String combination, List<String> combinationAll) {
        if (number.length() == 0) {
            combinationAll.add(combination);
        } else {
            char curNumber =number.charAt(0);
            String numberPhone = numberPhoneMap.get(curNumber);
            if (numberPhone == null) {
                printCharByPhone(number.substring(1), combination, combinationAll);
            } else {
                for (int i = 0; i < numberPhone.length(); i++) {
                    printCharByPhone(number.substring(1), combination + numberPhone.charAt(i), combinationAll);
                }
            }
        }
    }
}
