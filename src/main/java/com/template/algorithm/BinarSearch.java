package com.template.algorithm;

import java.util.List;

public class BinarSearch {
    public int binarySearch(int[] arr, int target) {
        int left = 0;
        int right = arr.length - 1;

        while (left <= right) {
            int mid = left + (right - left) / 2;

            if (arr[mid] == target) {
                return mid;
            } else if (arr[mid] < target) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }

        return -1;
    }

    /** Task 1
     * Вася решил накопить денег на два одинаковых велосипеда — себе и сестре. У Васи есть копилка, в которую каждый день он может добавлять деньги
     * (если, конечно, у него есть такая финансовая возможность). В процессе накопления Вася не вынимает деньги из копилки.
     * У вас есть информация о росте Васиных накоплений — сколько у Васи в копилке было денег в каждый из дней.
     * Ваша задача — по заданной стоимости велосипеда определить
     * первый день, в которой Вася смог бы купить один велосипед,
     * и первый день, в который Вася смог бы купить два велосипеда.
     * Подсказка: решение должно работать за O(log n).
     *
     * Пример 1
     * Ввод	                     Вывод
     * 6                         3 5
     * 1 2 4 4 6 8
     * 3
     * ---------------------------------------------
     * int b1 = searchBinar(0, len, arr, price);
     * int b2 = searchBinar(b1, len, arr, price * 2);
     * */
    private int searchBinaryTask1(int left, int right, List<Integer> arr, int price) {
        if (left >= right) {
            return -1;
        }
        int mid = (left + right) / 2;
        if (mid == 0 && arr.get(mid) >= price)
            return mid;

        if (arr.get(mid) >= price && (mid != 0 && arr.get(mid - 1) < price))
            return mid;

        if (arr.get(mid) >= price) {
            return searchBinaryTask1(left, mid, arr, price);
        } else {
            return searchBinaryTask1(mid + 1, right, arr, price);
        }
    }

    /** Task 10
     * Чтобы выбрать самый лучший алгоритм для решения задачи, Гоша продолжил изучать разные сортировки.
     * Её алгоритм следующий (сортируем по неубыванию):
     * На каждой итерации проходим по массиву, поочередно сравнивая пары соседних элементов. Если элемент на позиции i больше элемента на позиции i + 1,
     * меняем их местами. После первой итерации самый большой элемент всплывёт в конце массива.
     * Проходим по массиву, выполняя указанные действия до тех пор, пока на очередной итерации не окажется, что обмены больше не нужны, то есть массив уже отсортирован.
     * После не более чем n – 1 итераций выполнение алгоритма заканчивается, так как на каждой итерации хотя бы один элемент оказывается на правильной позиции.
     *
     * Пример 1
     * Ввод	                     Вывод
     * 5                         3 4 2 1 9
     * 4 3 9 2 1                 3 2 1 4 9
     *                           2 1 3 4 9
     *                           1 2 3 4 9
     * ---------------------------------------------
     * int b1 = searchBinar(0, len, arr, price);
     * int b2 = searchBinar(b1, len, arr, price * 2);
     * */
    private void sortBubble(List<Integer> numberList) {
        if (numberList.size() == 1)
            System.out.print(numberList.get(0));

        int delLen = 0;

        for (int index = 0; index < numberList.size(); index++) {
            boolean flag = false;

            for (int indexNum = 1; indexNum < numberList.size() - delLen; indexNum++) {
                int num = numberList.get(indexNum - 1);

                if (numberList.get(indexNum) < num) {
                    numberList.set(indexNum - 1, numberList.get(indexNum));
                    numberList.set(indexNum, num);
                    flag = true;
                }
            }

            if (!flag && index != 0)
                break;

            StringBuilder numberStr = new StringBuilder();
            numberList.forEach(e -> numberStr.append(e).append(" "));
            System.out.println(numberStr);
            delLen++;
        }
    }
}
