package com.template.algorithm;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class StackQueue {
    /**
     * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
     * An input string is valid if:
     * Open brackets must be closed by the same type of brackets.
     * Open brackets must be closed in the correct order.
     * Every close bracket has a corresponding open bracket of the same type.
     * Example
     * Input: s = "()[]{}"
     * Output: true*/
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        Set<Character> set = new HashSet<>(){{
            add('(');
            add('{');
            add('[');
        }};

        for(char c : s.toCharArray()) {
            if(set.contains(c)) {
                stack.push(c);
            } else if(c == ')' && !stack.isEmpty() && stack.peek() == '(') {
                stack.pop();
            } else if(c == '}' && !stack.isEmpty() && stack.peek() == '{') {
                stack.pop();
            } else if(c == ']' && !stack.isEmpty() && stack.peek() == '[') {
                stack.pop();
            } else {
                return false;
            }
        }

        return stack.isEmpty();
    }
}
