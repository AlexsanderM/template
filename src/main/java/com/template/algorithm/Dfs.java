package com.template.algorithm;

import java.util.*;

public class Dfs {
    /**
     * Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return the number of islands.
     * An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all
     * four edges of the grid are all surrounded by water.
     * Example 2:
     * Input: grid = [
     *   ["1","1","0","0","0"],
     *   ["1","1","0","0","0"],
     *   ["0","0","1","0","0"],
     *   ["0","0","0","1","1"]
     * ]
     * Output: 3
     * */
    public int numIslands(char[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        int count = 0;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') { // начинаем новый остров
                    count++;
                    dfs(grid, i, j); // поиск в глубину
                }
            }
        }
        return count;
    }

    private void dfs(char[][] grid, int i, int j) {
        int m = grid.length;
        int n = grid[0].length;

        if (i < 0 || j < 0 || i >= m || j >= n || grid[i][j] == '0') {
            return; // выходим из метода, если встретили воду или вышли за границы карты
        }

        grid[i][j] = '0'; // помечаем текущую клетку как посещенную

        dfs(grid, i+1, j); // идём вниз
        dfs(grid, i-1, j); // идём вверх
        dfs(grid, i, j+1); // идём вправо
        dfs(grid, i, j-1); // идём влево
    }

    /**
     * Given a string s that contains parentheses and letters, remove the minimum number of invalid parentheses to make the input string valid.
     * Return a list of unique strings that are valid with the minimum number of removals. You may return the answer in any order.
     * Example 1:
     * Input: s = "()())()"
     * Output: ["(())()","()()()"]
     * Example 2:
     * Input: s = "(a)())()"
     * Output: ["(a())()","(a)()()"]
     * Example 3:
     * Input: s = ")("
     * Output: [""]*/
    public List<String> removeInvalidParentheses(String s) {
        List<String> result = new ArrayList<>();

        if (s == null) {
            return result;
        }

        Queue<String> queue = new LinkedList<>();
        Set<String> visited = new HashSet<>();

        queue.offer(s);
        visited.add(s);

        boolean found = false;

        while (!queue.isEmpty()) {
            String current = queue.poll();

            if (isValid(current)) {
                result.add(current);
                found = true;
            }

            if (found) {
                continue;
            }

            for (int i = 0; i < current.length(); i++) {
                if (current.charAt(i) != '(' && current.charAt(i) != ')') {
                    continue;
                }

                String next = current.substring(0, i) + current.substring(i + 1);

                if (!visited.contains(next)) {
                    queue.offer(next);
                    visited.add(next);
                }
            }
        }

        return result;
    }

    private boolean isValid(String s) {
        int count = 0;

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == '(') {
                count++;
            } else if (c == ')') {
                count--;

                if (count < 0) {
                    return false;
                }
            }
        }

        return count == 0;
    }
}
