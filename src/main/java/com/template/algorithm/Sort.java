package com.template.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class Sort {
    // сортировки вставками (insertion sort)
    /**
     * Даны числа. Нужно определить, какое самое большое число можно из них составить.
     * Пример 1
     * Ввод	            Вывод
     * 3                56215
     * 15 56 2
     * ---------------------------------------------
     * if(list.size() == 1) {
     *   System.out.print(list.get(0));
     * } else
     *   sortByKey(list);
     * Временная сложность O(n^2), пространственная сложность O(1)
     *  */
    public void sortByKey(List<String> list) {
        for (int i = 1; i < list.size(); i++) {
            String itemToInsert = list.get(i);
            int j = i;

            while (j > 0) {
                String s1 = list.get(j) + list.get(j-1);
                String s2 = list.get(j-1) + list.get(j);
                if (s1.compareTo(s2) > 0) {
                    list.set(j, list.get(j - 1));
                    j--;
                    list.set(j, itemToInsert);
                } else {
                    break;
                }
            }

        }
    }

    // сортировки выбором (selection sort)
    /**
     * код сортирует массив методом сортировки выбором (selection sort). Алгоритм на каждом шаге находит наименьший элемент в оставшейся части массива,
     * затем меняет его местами с первым элементом в неотсортированной части массива.
     * В результате элементы в итоговом массиве будут отсортированы в порядке возрастания.
     * Сложность сортировки выбором во всех случаях составляет O(n^2).*/
    public static class SelectionSort {
        public static void main(String[] args) {
            int[] array = { 9, 7, 2, 5, 4, 1 };
            int n = array.length;

            for (int i = 0; i < n - 1; ++i) {
                int minIndex = i;

                for (int j = i + 1; j < n; ++j)
                    if (array[j] < array[minIndex])
                        minIndex = j;

                int temp = array[minIndex];
                array[minIndex] = array[i];
                array[i] = temp;
            }

            for (int i = 0; i < n; ++i)
                System.out.print(array[i] + " ");
        }
    }

    // сортировка слиянием (merge sort)
    public static void mergeSort(int[] arr, int left, int right) {
        if (left < right) {
            int middle = (left + right) / 2;
            mergeSort(arr, left, middle);
            mergeSort(arr, middle + 1, right);
            merge(arr, left, middle, right);
        }
    }
    public static void merge(int[] arr, int left, int middle, int right) {
        int n1 = middle - left + 1;
        int n2 = right - middle;

        int[] leftArr = new int[n1];
        int[] rightArr = new int[n2];

        for (int i = 0; i < n1; ++i)
            leftArr[i] = arr[left + i];
        for (int j = 0; j < n2; ++j)
            rightArr[j] = arr[middle + 1 + j];

        int i = 0, j = 0;

        int k = left;
        while (i < n1 && j < n2) {
            if (leftArr[i] <= rightArr[j]) {
                arr[k] = leftArr[i];
                i++;
            } else {
                arr[k] = rightArr[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = leftArr[i];
            i++;
            k++;
        }
        while (j < n2) {
            arr[k] = rightArr[j];
            j++;
            k++;
        }
    }

    /**
     * Алла захотела, чтобы у неё под окном были узкие клумбы с тюльпанам. На схеме земельного участка клумбы обозначаются просто горизонтальными отрезками,
     * лежащими на одной прямой. Для ландшафтных работ было нанято n садовников. Каждый из них обрабатывал какой-то отрезок на схеме. Процесс был организован
     * не очень хорошо, иногда один и тот же отрезок или его часть могли быть обработаны сразу несколькими садовниками. Таким образом, отрезки, обрабатываемые
     * двумя разными садовниками, сливаются в один. Непрерывный обработанный отрезок затем станет клумбой. Нужно определить границы будущих клумб.
     * Рассмотрим примеры.
     * Пример 1:
     * Даны 4 отрезка:
     * [7,8],[7,8],[2,3],[6,10]. Два одинаковых отрезка
     * [7,8] и [7,8] сливаются в один, но потом их накрывает отрезок
     * [6,10]. Таким образом, имеем две клумбы с координатами
     * [2,3] и [6,10] * */
    // 1
    public List<Segment> mergeSort(List<Segment> list) {
        if (list.size() < 2) {
            return list;
        }
        int mid = list.size() / 2;

        List<Segment> left = mergeSort(list.subList(0, mid));
        List<Segment> right = mergeSort(list.subList(mid, list.size()));
        List<Segment> result = merge(left, right);

        ArrayList<Segment> merged = new ArrayList<>();
        merged.add(result.get(0));

        for (int k = 1; k < result.size(); k++) {
            Segment current = result.get(k);
            Segment last = merged.get(merged.size() - 1);

            if (current.start <= last.end && last.start <= current.end) {
                int start = Math.min(last.start, current.start);
                int end = Math.max(last.end, current.end);
                last.start = start;
                last.end = end;
            } else {
                merged.add(current);
            }
        }

        return merged;
    }

    private List<Segment> merge(List<Segment> left, List<Segment> right) {
        List<Segment> list = new ArrayList<>();
        int l = 0, r = 0;
        while (l < left.size() && r < right.size()) {
            if (left.get(l).start <= right.get(r).start) {
                list.add(left.get(l));
                l++;
            } else {
                list.add(right.get(r));
                r++;
            }
        }

        while (l < left.size()) {
            list.add(left.get(l));
            l++;
        }
        while (r < right.size()) {
            list.add(right.get(r));
            r++;
        }

        return list;
    }

    static class Segment {
        int start;
        int end;

        Segment(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    //------------------
    // 2
    public HashSet<Segment> merge(List<Segment> list) {
        HashSet<Segment> merged = new HashSet<>();

        for (int i = 0; i < list.size(); i++) {
            int start = list.get(i).start;
            int end = list.get(i).end;
            Segment segment = new Segment(start, end);

            for (Segment s : merged) {
                if (s.start <= segment.end && segment.start <= s.end) {
                    merged.remove(s);
                    start = Math.min(s.start, segment.start);
                    end = Math.max(s.end, segment.end);
                    segment = new Segment(start, end);
                }
            }

            merged.add(segment);
        }

        return merged;
    }

    //Сортировка подсчётом (англ. counting sort)
    //O(n+k), где k — количество различных элементов
    /**
     * Рита решила оставить у себя одежду только трёх цветов: розового, жёлтого и малинового. После того как вещи других расцветок были убраны, Рита захотела
     * отсортировать свой новый гардероб по цветам. Сначала должны идти вещи розового цвета, потом —– жёлтого, и в конце —– малинового. Помогите Рите справиться
     * с этой задачей.
     * Пример 1
     * Ввод	            Вывод
     * 7
     * 0 2 1 2 0 0 1    0 0 0 1 1 2 2
     * */
    public int[] countSort(int[] arr, int k) {
        int[] temp = new int[k];
        for (int i = 0; i < arr.length; i++) {
            temp[arr[i]]++;
        }

        int index = 0;
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < temp[i]; j++) {
                arr[index] = i;
                index++;
            }
        }

        return arr;
    }
}
