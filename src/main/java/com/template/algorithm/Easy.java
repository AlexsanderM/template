package com.template.algorithm;

import java.util.Stack;

public class Easy {
    /**
     * Дана скобочная последовательность. Нужно определить, правильная ли она.
     * Будем придерживаться такого определения:
     * пустая строка —– правильная скобочная последовательность;
     * правильная скобочная последовательность, взятая в скобки одного типа, –— правильная скобочная последовательность;
     * правильная скобочная последовательность с приписанной слева или справа правильной скобочной последовательностью —– тоже правильная.
     * На вход подаётся последовательность из скобок трёх видов: [], (), {}.
     * Напишите функцию is_correct_bracket_seq, которая принимает на вход скобочную последовательность и возвращает True, если последовательность правильная,
     * а иначе False.*/
    public void isCorrectBracketSeq(String str) {
        Stack<Integer> arrStack = new Stack<>();
        boolean flag = true;

        for (int i = 0; i < str.length(); i++) {
            int num = str.charAt(i);
            if (num == 40 || num == 91 || num == 123) {
                arrStack.push(num);
            } else {
                if (arrStack.size() != 0) {
                    Integer pop = arrStack.pop();
                    if (num - pop != 1 && num - pop != 2) {
                        flag = false;
                        break;
                    }

                } else {
                    flag = false;
                    break;
                }

            }
        }

        if (arrStack.size() == 0 && flag) {
            System.out.println("True");
        } else
            System.out.println("False");
    }

    /**
     * Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer
     * range [-231, 231 - 1], then return 0.
     * Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
     * Example
     * Input: x = 123
     * Output: 321
     * */
    public int reverse(int x) {
        boolean flag = true;
        if(x < 0) {
            x *= -1;
            flag = false;
        }

        long num = 0;
        while(x > 0) {
            int temp = x % 10;
            x /= 10;
            if(num == 0) {
                if(temp != 0)
                    num = temp;
            } else {
                num *= 10;
                num += temp;
            }
        }

        if(num >= Integer.MAX_VALUE || num <= Integer.MIN_VALUE) {
            return 0;
        }

        return flag ? (int)num : (int)-num;
    }

    /**
     * The algorithm for myAtoi(string s) is as follows:
     * Read in and ignore any leading whitespace.
     * Check if the next character (if not already at the end of the string) is '-' or '+'. Read this character in if it is either. This determines if the
     * final result is negative or positive respectively. Assume the result is positive if neither is present.
     * Read in next the characters until the next non-digit character or the end of the input is reached. The rest of the string is ignored.
     * Convert these digits into an integer (i.e. "123" -> 123, "0032" -> 32). If no digits were read, then the integer is 0. Change the sign as necessary
     * (from step 2).
     * If the integer is out of the 32-bit signed integer range [-231, 231 - 1], then clamp the integer so that it remains in the range. Specifically,
     * integers less than -231 should be clamped to -231, and integers greater than 231 - 1 should be clamped to 231 - 1.
     * Return the integer as the final result.
     * Example
     * Input: s = "   -42"
     * Output: -42
     * Input: s = "4193 with words"
     * Output: 4193
     * */
    public int myAtoi(String s) {
        boolean negativ = true;
        long num = 0;
        s=s.trim();
        if(s.length() > 0) {
            if(s.charAt(0) == '-') {
                negativ = false;
            }
            int index = s.charAt(0) == '-' || s.charAt(0) == '+' ? 1 : 0;
            for(int i = index; i < s.length(); i++) {
                if(i == index && (s.charAt(i) < '0' || s.charAt(i) > '9')) {
                    return 0;
                }
                if(s.charAt(i) >= '0' && s.charAt(i) <= '9') {
                    num = num * 10 + (s.charAt(i) - '0');
                } else {
                    break;
                }
                if(num > Integer.MAX_VALUE) {
                    return negativ ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                }
            }
            num = negativ ? num : -num;
        }

        return (int)num;
    }
}
