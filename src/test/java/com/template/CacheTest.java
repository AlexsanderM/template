package com.template;

import com.template.cache.User;
import com.template.cache.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CacheTest {
    @Autowired
    private UserService service;

    @Test
    public void get() {
        User user1 = service.create(new User(6L, "Vasya", "vasya@mail.ru"));
        User user2 = service.create(new User(1L, "Kolya", "kolya@mail.ru"));

        getAndPrint(user1.getId());
        getAndPrint(user2.getId());
        getAndPrint(user1.getId());
        getAndPrint(user2.getId());
    }

    private void getAndPrint(Long id) {
        System.out.println("user found: - " + service.get(id));
    }
}
